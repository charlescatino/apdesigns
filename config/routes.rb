Rails.application.routes.draw do
  root 'welcome#home'
  get '/contact' => 'welcome#contact'
  get '/images' => 'welcome#images'
  get '/groupsales/axo-alphakappa' => 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
