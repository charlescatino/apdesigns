class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  @showCart = true
end
