require 'square_connect'
require 'unirest'

class WelcomeController < ApplicationController
  def home
    @hideCart = true
  end
  
  def index
    @quantity_options = (1..25).to_a.map{|s| ["#{s}", s]}
    @month_options = [["01 - JAN", "01"], ["02 - FEB", "02"], ["03 - MAR", "03"],
		["04 - APR", "04"], ["05 - MAY", "05"], ["06 - JUN", "06"],
		["07 - JUL", "07"], ["08 - AUG", "08"], ["09 - SEP", "09"],
	    ["10 - OCT", "10"], ["11 - NOV", "11"], ["12 - DEC", "12"]]
    @year_options = ((Time.now.year)..(Time.now.year+8)).to_a.map{|y| ["#{y}", y]}


  	@access_token = 'sandbox-sq0atb-ZG3tsAW4lU1pGpLwGbfaIA'
    # test_square_connect_locations
	# test_square_connect_payment
	# test_unirest_locations
	# test_unirest_payment
  end

  def contact
  	@subject = params[:subject]
  	puts @subject
    @hideCart = true
    UserMailer.contact_email().deliver
  end

  def test_square_connect_locations
    locationApi = SquareConnect::LocationApi.new()
    begin
      locations = locationApi.list_locations(@access_token)
      puts locations
    rescue
      puts 'error'
    end
  end

  def test_square_connect_payment
  	# Assume you have correct values assigned to the following variables:
	#   NONCE
	#   LOCATION_ID
	#   ACCESS_TOKEN
	location_id = 'CBASEGPUE-_3ZNP9tMLkXDeVvdY'
	nonce = 'CBASEGZTuB79WiwXUEIPvrXw_as'

	transaction_api = SquareConnect::TransactionApi.new()

	request_body = {
	  
	  :card_nonce => nonce,

	  # Monetary amounts are specified in the smallest unit of the applicable currency.
	  # This amount is in cents. It's also hard-coded for $1, which is not very useful.
	  :amount_money => {
	    :amount => 100,
	    :currency => 'USD'
	  },

	  # Every payment you process for a given business have a unique idempotency key.
	  # If you're unsure whether a particular payment succeeded, you can reattempt
	  # it with the same idempotency key without worrying about double charging
	  # the buyer.
	  :idempotency_key => SecureRandom.uuid
	}


	# The SDK throws an exception if a Connect endpoint responds with anything besides 200 (success).
	# This block catches any exceptions that occur from the request.
	begin
	  resp = transaction_api.charge(@access_token, location_id, request_body)
	rescue SquareConnect::ApiError => e
	    puts 'Error encountered while charging card:'
	    puts e.message
	end
	puts resp
  end

  def test_unirest_locations
  	response = Unirest.get "http://connect.squareup.com/v2/locations", 
                        headers:{ 'Authorization': 'Bearer ' + @access_token,
				                   'Accept':        'application/json',
				                   'Content-Type':  'application/json' }, 
                        parameters:{ }

	puts response.code # Status code
	puts response.headers # Response headers
	puts response.body # Parsed body
	puts response.raw_body # Unparsed body
  end

  def test_unirest_payment
	location_id = 'CBASEGPUE-_3ZNP9tMLkXDeVvdY'
	nonce = 'CBASEGZTuB79WiwXUEIPvrXw_as'

	response = Unirest.post "https://connect.squareup.com/v2/locations/" + location_id + "/transactions",
      headers:{ 'Authorization': 'Bearer ' + @access_token,
	             'Accept':        'application/json',
	             'Content-Type':  'application/json' },
	  parameters: {
		  note: 'aptional note',
	    idempotency_key: SecureRandom.uuid,
	    amount_money: {
	      amount: 100,
	      currency: 'USD'
	    },
	    card_nonce: nonce,
	    shipping_address: {
		    address_line_1: '123 Main St',
		    locality: 'San Francisco',
		    administrative_district_level_1: 'CA',
		    postal_code: '94114',
		    country: 'US'
		  },
		  billing_address: {
		    address_line_1: '500 Electric Ave',
		    address_line_2: 'Suite 600',
		    administrative_district_level_1: 'NY',
		    locality: 'New York',
		    postal_code: '20003',
		    country: 'US'
		  },
		  reference_id: 'optional reference #112358'
	  }

	puts response.code # Status code
	puts response.headers # Response headers
	puts response.body # Parsed body
	puts response.raw_body # Unparsed body


	# # The SDK throws an exception if a Connect endpoint responds with anything besides 200 (success).
	# # This block catches any exceptions that occur from the request.
	# begin
	#   resp = transaction_api.charge(@access_token, location_id, request_body)
	# rescue SquareConnect::ApiError => e
	#     puts 'Error encountered while charging card:'
	#     puts e.message
	# end
	# puts resp
  end
end